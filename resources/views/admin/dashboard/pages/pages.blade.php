@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Pages
                    </h2>
                </div>
                <div class="col-auto d-print-none">
                    <div class="btn-list">
                        <a href="/pages/create" class="btn btn-primary d-inline-block">
                            <i class="fa fa-plus me-2"></i> Buat Halaman Baru
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col mb-3 mb-md-0 ">

                </div>
                <div class="col-auto">
                    <div class="row gx-1">

                        <div class="col-auto">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search..." id="search">
                                <div class="input-group-append">
                                    <button class="btn btn-white btn-icon" disabled>Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col-auto mb-3 mb-md-0">
                    <div class="row gx-1">
                        <div class="col">
                        </div>
                        <div class="col-auto">

                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="row gx-2 align-items-center">
                        
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
                    @if (session('success'))
                        <div class="alert alert-success d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                                aria-label="Danger:">
                                <use xlink:href="#exclamation-triangle-fill" />
                            </svg>
                            <div class="text-center">
                                {{ session('success') }}
                            </div>
                        </div>
                    @endif
                    <table class="table card-table table-post" id="myTable">
                        <thead>
                            <tr>
                                {{-- <th width="10"><input type="checkbox" class="checkall"></th> --}}
                                <th width="10">Title</th>
                                <th width="200">URL</th>
                                <th width="200">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($page as $pages)
                                <tr>
                                    {{-- <td><input type="checkbox"></td> --}}
                                    <td>
                                        <a href="/pages/{{ $pages->id }}/edit"
                                            class="font-weight-bold">{{ $pages->judul }}</a>
                                        <div class="action text-muted">
                                            <a href="/pages/{{ $pages->id }}/edit">Edit</a>
                                            <div class="vr mx-1"></div>
                                            {{-- <a href="#" class="text-danger btn-delete">Trash</a> --}}
                                            <a style="color:black" href="{{ route('pages.destroy', $pages->id) }}"
                                                onclick="event.preventDefault(); document.getElementById('delete-form-{{ $pages->id }}').submit();">
                                                Trash
                                            </a>
                                            <form id="delete-form-{{ $pages->id }}"
                                                action="{{ route('pages.destroy', $pages->id) }}" method="POST"
                                                style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                            <div class="vr mx-1"></div>
                                            <a href="/ANRI/{{ $pages->nama_menu }}">View</a>
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{ $pages->nama_menu }}</div>
                                    </td>
                                    <td>

                                        <div>{{ $pages->created_at }}</div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Pages")').parents(
                '.nav-item').addClass('active');
        });
    </script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('.btn-delete').on('click', function() {
                Swal.fire({
                    title: 'Hapus Data?',
                    text: "Apakah anda yakin?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Success!',
                            'Data Berhasil dihapus',
                            'success'
                        )
                    }
                })
            });
        });
    </script>
@endsection
