@extends('admin.main')
{{-- @dd($static_submenu) --}}
@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <div class="page-pretitle">
                        Manajemen Menu
                    </div>
                    <h2 class="page-title">
                        Main Menu
                    </h2>
                </div>
                <div class="col-auto d-print-none">
                    <div class="btn-list">
                        <a href="/manajemen-menu/create" class="btn btn-primary d-inline-block">
                            <i class="fa fa-plus me-2"></i> Tambah Menu Navbar
                        </a>
                        <a href="/pages/create" class="btn btn-yellow">
                            <i class="fa fa-plus me-2"></i> Tambah Menu Content
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col-auto mb-3 mb-md-0">
                    <div class="row gx-1">
                        <div class="col">
                            <div class="page-pretitle">
                                Table Data
                            </div>
                            <h2 class="page-title">
                                Menu Navbar
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
                    @if (session('successss'))
                        <div class="alert alert-success d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                                aria-label="Danger:">
                                <use xlink:href="#exclamation-triangle-fill" />
                            </svg>
                            <div class="text-center text-dark">
                                {{ session('successss') }}
                            </div>
                        </div>
                    @endif
                    <table class="table card-table table-post" id="myTable">
                        <thead>
                            <tr>    
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($static_adds->skip(9) as $menu)
                                <tr>
                                    <td>
                                        <a href="/update-menu-navbar/{{ $menu->id }}/edit" class="font-weight-bold">
                                            {{ $menu->name }}
                                        </a>
                                        <div class="action text-muted">
                                            <a href="/update-menu-navbar/{{ $menu->id }}/edit">Edit</a>
                                            <div class="vr mx-1"></div>
                                            <a style="color:black" href="{{ route('manajemen-menu.destroy', $menu->id) }}"
                                                onclick="event.preventDefault(); document.getElementById('delete-form-{{ $menu->id }}').submit();">
                                                Trash
                                            </a>
                                            <form id="delete-form-{{ $menu->id }}"
                                                action="{{ route('manajemen-menu.destroy', $menu->id) }}" method="POST"
                                                style="display: none;">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        </div>
                                    </td>
                                    {{-- <td>{{ $static_adds[3]->url }}</td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="page-body">
        <div class="container-xl">
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col-auto mb-3 mb-md-0">
                    <div class="row gx-1">
                        <div class="col">
                            <div class="page-pretitle">
                                Table Data
                            </div>
                            <h2 class="page-title">
                                Menu Hyperlink Utama
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
                    @if (session('successs'))
                        <div class="alert alert-success d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                                aria-label="Danger:">
                                <use xlink:href="#exclamation-triangle-fill" />
                            </svg>
                            <div class="text-center text-dark">
                                {{ session('successs') }}
                            </div>
                        </div>
                    @endif
                    <table class="table card-table table-post" id="myTable">
                        <thead>
                            <tr>
                                <th>Menu</th>
                                <th>Url</th>
                                {{-- <th>URL</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="/update-menu/{{ $static_adds[3]->id }}/edit" class="font-weight-bold">
                                        {{ $static_adds[3]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-menu/{{ $static_adds[3]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_adds[3]->url }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/update-submenu/{{ $static_submenu[8]->id }}/edit" class="font-weight-bold">
                                        {{ $static_submenu[8]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-submenu/{{ $static_submenu[8]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_submenu[8]->url }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/update-submenu/{{ $static_submenu[9]->id }}/edit" class="font-weight-bold">
                                        {{ $static_submenu[9]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-submenu/{{ $static_submenu[9]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_submenu[9]->url }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/update-submenu/{{ $static_submenu[10]->id }}/edit" class="font-weight-bold">
                                        {{ $static_submenu[10]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-submenu/{{ $static_submenu[10]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_submenu[10]->url }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/update-submenu/{{ $static_submenu[11]->id }}/edit" class="font-weight-bold">
                                        {{ $static_submenu[11]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-submenu/{{ $static_submenu[11]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_submenu[11]->url }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/update-submenu/{{ $static_submenu[16]->id }}/edit" class="font-weight-bold">
                                        {{ $static_submenu[16]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-submenu/{{ $static_submenu[16]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_submenu[16]->url }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/update-submenu/{{ $static_submenu[17]->id }}/edit" class="font-weight-bold">
                                        {{ $static_submenu[17]->name }}
                                    </a>
                                    <div class="action text-muted">
                                        <a href="/update-submenu/{{ $static_submenu[17]->id }}/edit">Edit</a>
                                        <div class="vr mx-1"></div>
                                        
                                    </div>
                                </td>
                                <td>{{ $static_submenu[17]->url }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Manajemen Menu")')
                .parents('.nav-item').addClass('active').find('.dropdown-menu').addClass('show').find(
                    '.dropdown-item:contains("Menu Utama")').addClass('active');
        });
    </script>
    <link rel="stylesheet" type="text/css"
        href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.js"></script>
@endsection
