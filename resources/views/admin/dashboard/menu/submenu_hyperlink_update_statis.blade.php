@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Edit Sub Menu Hyperlink
                    </h2>
                </div>
                <div class="col-auto d-print-none">
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row">
                <form action="/manajemen-submenu-hyperlink/{{ $submenus->id }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="mb-3">
                            <label for="name" class="form-label">Nama Menu</label>
                            <input type="text" id="name" name="name" value="{{ old('name') ?? $submenus->name }}"
                                class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Menu">
                            @error('name')
                                <div class="invalid-feedback">
                                    Nama menu harus di isi terlebih dahulu!
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="url" class="form-label">Url</label>
                            <input type="text" id="name" name="url" value="{{ old('url') ?? $submenus->url }}"
                                class="form-control @error('url') is-invalid @enderror" placeholder="Masukkan URL">
                            @error('url')
                                <div class="invalid-feedback">
                                    Url harus di isi terlebih dahulu!
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <div class="card-header font-weight-bold">Menu Parent</div>
                            <div class="card-body">
                                <div class="mb-2">
                                    <select name="menu_id"
                                        class="form-control @error('menu_id') is-invalid @enderror">
                                        <option value="{{ $submenus->menus->id }}" selected>{{ $submenus->menus->name }}</option>
                                        <option value="{{ $menus[1]->id }}">{{ $menus[1]->name }}
                                        </option>
                                        <option value="{{ $menus[2]->id }}">{{ $menus[2]->name }}
                                        </option>
                                        <option value="{{ $menus[4]->id }}">{{ $menus[4]->name }}
                                        </option>
                                        <option value="{{ $menus[5]->id }}">{{ $menus[5]->name }}
                                        </option>
                                        @foreach ($menus->skip(9) as $menu)
                                            <option value="{{ $menu->id }}">{{ $menu->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('menu_id')
                                        <div class="invalid-feedback">
                                            Menu harus di isi terlebih dahulu!
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-publish btn-primary mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function() {
                $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Manajemen Menu")')
                    .parents('.nav-item').addClass('active').find('.dropdown-menu').addClass('show').find(
                        '.dropdown-item:contains("Sub Menu Hyperlink")').addClass('active');
            });
        </script>
    @endsection
