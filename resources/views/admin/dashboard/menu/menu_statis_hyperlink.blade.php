@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Edit Menu 
                    </h2>
                </div>
                <div class="col-auto d-print-none">
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row">
                <form action="/update-menu-hyperlink/{{ $menus->id }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="mb-3">
                            <label for="name" class="form-label">Nama Menu</label>
                            <input type="text" id="name" name="name" value="{{ old('name') ?? $menus->name }}"
                                class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Menu">
                            @error('name')
                                <div class="invalid-feedback">
                                    Nama menu harus di isi terlebih dahulu!
                                </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="url" class="form-label">Url</label>
                            <input type="text" id="name" name="url" value="{{ old('url') ?? $menus->url }}"
                                class="form-control @error('url') is-invalid @enderror" placeholder="Masukkan URL">
                            @error('url')
                                <div class="invalid-feedback">
                                    Url harus di isi terlebih dahulu!
                                </div>
                            @enderror
                        </div>
                        <button class="btn btn-publish btn-primary mt-2">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Manajemen Menu")')
                .parents('.nav-item').addClass('active').find('.dropdown-menu').addClass('show').find(
                    '.dropdown-item:contains("Menu Hyperlink")').addClass('active');
        });
    </script>

@endsection
