@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Manajemen User
                    </h2>
                </div>
                <div class="col-auto d-print-none">
                    <div class="btn-list">
                        <a href="/manajemen-user/create" class="btn btn-primary d-inline-block">
                            <i class="fa fa-plus me-2"></i> Tambah User
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col-auto mb-3 mb-md-0">
                    <div class="row gx-1">
                        <div class="col">
                            <div class="col">
                                <h2 class="page-title">
                                    Filter User
                                </h2>
                            </div>
                            <select id="status" class="form-control">
                                <option value="" selected>Pilih Filter User</option>
                                <option value="0">User Tidak Aktif</option>
                                <option value="1">User Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col mb-3 mb-md-0 ">
                </div>
                <div class="col-auto">
                    <div class="row gx-1">
                        <div class="col-auto">
                            <div class="input-group">
                                <input type="text" class="form-control" id="search" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn btn-white btn-icon">Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between align-items-center mb-3">
                <div class="col-auto mb-3 mb-md-0">
                    <div class="row gx-1">
                        <button id="destroy" class="btn btn-danger d-none">
                            <i class="bi bi-trash me-2"></i>Hapus
                        </button>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="row gx-2 align-items-center">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
                    @if (session('success'))
                        <div class="alert alert-success d-flex align-items-center" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                                aria-label="Danger:">
                                <use xlink:href="#exclamation-triangle-fill" />
                            </svg>
                            <div class="text-center">
                                {{ session('success') }}
                            </div>
                        </div>
                    @endif
                    <table class="table card-table table-post" id="datatable">
                        <thead>
                            <tr>
                                <th width="10"><input type="checkbox" class="checkall"></th>
                                <th>Aksi</th>
                                <th>Username</th>
                                <th>Nama</th>
                                <th>Role</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Manajemen User")')
                .parents('.nav-item').addClass('active');
        });
    </script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $(function() {
            var table = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                paging: true,
                lengthChange: true,
                searching: false,
                ordering: true,
                info: true,
                autoWidth: false,
                responsive: true,
                language: {
                    search: "",
                    searchPlaceholder: "Masukkan kata pencarian...",
                },
                ajax: {
                    url: "/manajemen-user",
                    data: function(d) {
                        d.status = $('#status').val(),
                            d.search = $('#search').val()
                    }
                },

                columns: [{
                        data: "checkbox",
                        name: "checkbox",
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: "action",
                        name: "action",
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: "username",
                        name: "username",
                    },
                    {
                        data: "name",
                        name: "name",
                    },
                    {
                        data: "role",
                        name: "role",
                    },
                    {
                        data: "status",
                        name: "status",
                    },
                ],
            });
        });


        $('#status').change(function() {
            $("#datatable").DataTable().ajax.reload(null, true);
        });

        $('#search').change(function() {
            $("#datatable").DataTable().ajax.reload(null, true);
        });

        $(document).on("click", 'input[name="all"]', function() {
            if (this.checked) {
                $('input[name="id"]').each(function() {
                    this.checked = true;
                });
            } else {
                $('input[name="id"]').each(function() {
                    this.checked = false;
                });
            }
            toggleDestroyButton();
        });

        $(document).on("change", 'input[name="id"]', function() {
            if ($('input[name="id"]').length == $('input[name="id"]:checked').length) {
                $('input[name="all"]').prop("checked", true);
            } else {
                $('input[name="all"]').prop("checked", false);
            }
            toggleDestroyButton();
        });

        function toggleDestroyButton() {
            if ($('input[name="id"]:checked').length > 0) {
                $("button#destroy")
                    .text(
                        "Delete (" + $('input[name="id"]:checked').length + ")"
                    )
                    .removeClass("d-none");
            } else {
                $("button#destroy").addClass("d-none");
            }
        }

        $(document).on('click', 'button#destroy', function() {
            var files = [];
            $('input[name="id"]:checked').each(function() {
                files.push($(this).data('id'));
            });

            if (files.length > 0) {
                console.log(files);
                Swal.fire({
                    title: 'Konfirmasi',
                    html: 'Apakah anda yakin akan menghapus <b>(' + files.length + ')</b> data',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 400,
                    allowOutsideClick: false
                }).then(function(result) {
                    if (result.value) {
                        let url = '{{ route('delete-user-mass') }}';
                        $.ajax({
                            url: url,
                            type: "delete",
                            data: {
                                data: files
                            },
                            success: function(data) {
                                $("#datatable").DataTable().ajax.reload(null, true);
                                Swal.fire(
                                    'Informasi!',
                                    'Berhasil menghapus data',
                                    'success'
                                )
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                console.log("Gagal!", "Harap coba lagi");
                            }
                        });
                        $("button#destroy").addClass("d-none");
                    }
                })
            }
        });
    </script>
@endsection
