@extends('admin.main')
{{-- @dd($monthly[0]['pengunjung']) --}}
@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <!-- Page pre-title -->
                    <div class="page-pretitle">
                        Dashboard
                    </div>
                    <h2 class="page-title">
                        Overview @if (request()->input('start_date') && request()->input('end_date'))
                            {{ request()->input('start_date') }} - {{ request()->input('end_date') }}
                        @endif
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <form action="/dashboard-filter">
                @csrf
                <div class="row justify-content-between mb-3 d-print-none">
                    <div class="col mb-3 mb-md-0">
                        <div class="input-group date" id="datepicker">
                            <input type="text" class="form-control" id="start_date" name="start_date"
                                value="{{ old('start_date') }}" autocomplete="off" placeholder="Pilih tanggal awal">
                            <span class="input-group-append">
                                <span class="input-group-text bg-light d-block">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col mb-3 mb-md-0">
                        <div class="input-group date" id="enddatepicker">
                            <input type="text" class="form-control" id="end_date" name="end_date"
                                value="{{ old('end_date') }}" autocomplete="off" placeholder="Pilih tanggal akhir   ">
                            <span class="input-group-append">
                                <span class="input-group-text bg-light d-block">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col mb-3 mb-md-0">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
            </form>
            <div class="col-auto">
                <button onclick="window.print();" type="submit" class="btn btn-yellow">Print &nbsp; <span><i
                            class="fa fa-print" aria-hidden="true"> </i> </span></button>
                <span>

                </span>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-4 d-print-none">
            <div class="col mb-3 d-print-none">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <span class="bg-info text-white avatar">
                                    <i class="fa fa-file-signature"></i>
                                </span>
                            </div>
                            <div class="col">
                                <div class="font-weight-medium h2 m-0">
                                    {{ $berita }}
                                </div>
                                <div class="text-muted">
                                    Berita
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <span class="bg-info text-white avatar">
                                    <i class="fa fa-file-alt"></i>
                                </span>
                            </div>
                            <div class="col">
                                <div class="font-weight-medium h2 m-0">
                                    {{ $pengumuman }}
                                </div>
                                <div class="text-muted">
                                    Pengumuman
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <span class="bg-info text-white avatar">
                                    <i class="fa fa-file-alt"></i>
                                </span>
                            </div>
                            <div class="col">
                                <div class="font-weight-medium h2 m-0">
                                    {{ $artikel }}
                                </div>
                                <div class="text-muted">
                                    Artikel
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <span class="bg-info text-white avatar">
                                    <i class="fa fa-file-alt"></i>
                                </span>
                            </div>
                            <div class="col">
                                <div class="font-weight-medium h2 m-0">
                                    {{ $infografis }}
                                </div>
                                <div class="text-muted">
                                    Infografis
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-md-2">
        </div>

    </div>
    <div class="container-xl">
        <div class="card mb-3">
            <div class="card-header border-0 font-weight-bold">Grafik Pengunjung</div>
            <div class="card-body">
                <div id="chart-pengunjung" style="height: 250px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">Pengunjung</div>
                                <div class="h3">{{ $total_users }}</div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">Pengguna Sistem</div>
                                <div class="h3">{{ $users }}</div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">Total Hits</div>
                                <div class="h3">{{ isset($dataHits[0]['hits']) ? $dataHits[0]['hits'] : 0 }}</div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">Halaman/ Pengunjung</div>
                                <div class="h3">{{ isset($page[0]['page']) ? $page[0]['page'] : 0 }}</div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">Durasi Pengunjung Rata-Rata</div>
                                <div class="h3">
                                    {{ $times }} Menit
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">Penggunjung Aktif 30 hari</div>
                                <div class="h3">{{ $monthly[0]['pengunjung'] }}</div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="text-muted">% Pengunjung Baru</div>
                                <div class="h3">{{ isset($userTypes[0]['userNew']) ? $userTypes[0]['userNew'] : 0 }} %
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="card h-100">
                    <div class="card-header border-0 font-weight-bold text-center">Returning vs New</div>
                    <div class="card-body">
                        <div id="chart-pie" style="height:250px;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row d-print-none">
            <div class="col-md-3 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="text-muted">
                            Penggunjung Aktif 1 hari
                        </div>
                        <div class="font-weight-medium h2 m-0">
                            {{ $perday[0]['pengunjung'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="text-muted">
                            Penggunjung Aktif 7 hari
                        </div>
                        <div class="font-weight-medium h2 m-0">
                            {{ $weekly[0]['pengunjung'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="text-muted">
                            Penggunjung Aktif 14 hari
                        </div>
                        <div class="font-weight-medium h2 m-0">
                            {{ $twoweeks[0]['pengunjung'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="text-muted">
                            Penggunjung Aktif 30 hari
                        </div>
                        <div class="font-weight-medium h2 m-0">
                            {{ $monthly[0]['pengunjung'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 mb-3">
                <div class="card h-100 flex-row align-items-center">
                    <div class="card-body text-center">
                        <div class="h4">Pengguna Online Hari Ini</div>
                        <div class="display-4">{{ $perday[0]['pengunjung'] }}</div>
                        <div class="text-muted">Pengguna Aktif di situs hari ini</div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 mb-3">
                <div class="card h-100">
                    <div class="card-header border-0 font-weight-bold">Daftar Pengunjung Berdasarkan Kota</div>
                    <div class="table-responsive">
                        <table class="table card-table table-vcenter">
                            <thead>
                                <tr>
                                    <th>Kota</th>
                                    <th>Pengunjung</th>
                                    <th>% Pengunjung</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($kota))
                                    @foreach ($kota as $key => $value)
                                        <tr>
                                            <td>{{ $value['kota'] }}</td>
                                            <td>{{ $value['pengunjung'] }}</td>
                                            <td valign="middle">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar"
                                                        style="width: {{ $value['persentase'] }}%;"
                                                        aria-valuenow="{{ $value['persentase'] }}" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Dashboard")').parents(
                '.nav-item').addClass('active');
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $("#datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).on('changeDate', function(selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#enddatepicker').datepicker('setStartDate', minDate);
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $("#enddatepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).on('changeDate', function(selected) {
                var maxDate = new Date(selected.date.valueOf());
                $('#datepicker').datepicker('setEndDate', maxDate);
            });
        });
    </script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript">
        let results = JSON.parse(`{!! json_encode($dataPerMonth) !!}`);
        let bulan = results.map((d) => {
            return d.bulan;
        });
        let pengunjung = results.map((d) => {
            return d.pengunjung;
        });

        Highcharts.chart('chart-pengunjung', {
            chart: {
                type: 'area'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                area: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Data',
                data: pengunjung

            }]
        });

        let userTypes = JSON.parse(`{!! json_encode($userTypes) !!}`);

        Highcharts.chart('chart-pie', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Pengunjung',
                colorByPoint: true,
                data: [{
                    name: 'Returning Visitor',
                    y: userTypes[0]['userOld']
                }, {
                    name: 'New Visitor',
                    y: userTypes[0]['userNew']
                }]
            }]
        });
    </script>
@endsection
