@extends('admin.main')

@section('content')
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Jenis Diklat
                    </h2>
                </div>
                <div class="col-auto d-print-none">

                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row">
                <form action="/update-jenis/{{ $jenis->id }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="col-md-8 mb-4 mb-md-0">
                        <div class="mb-3">
                            <label for="" class="form-label">Jenis Diklat (Id)</label>
                            <input type="text" id="nama_jenis" name="nama_jenis" class="form-control @error('nama_jenis') is-invalid @enderror"
                                placeholder="Masukkan Jenis" value="{{ $jenis->nama_jenis }}">
                            @error('nama_jenis')
                                <div class="invalid-feedback">
                                    Nama jenis diklat harus di isi terlebih dahulu!
                                </div>
                            @enderror
                        </div>
                        <button class="btn btn-publish  btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#navbar-menu > .navbar-nav > .nav-item > .nav-link > .nav-link-title:contains("Program Diklat")')
                .parents('.nav-item').addClass('active').find('.dropdown-menu').addClass('show').find(
                    '.dropdown-item:contains("Jenis & Jenjang")').addClass('active');
        });
    </script>

@endsection
