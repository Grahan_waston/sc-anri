<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->id();
            $table->string('nama_website');
            $table->string('alamat');
            $table->string('no_telfon');
            $table->string('no_whatsapp');
            $table->string('email_pertama');
            $table->string('email_kedua');
            $table->text('maps');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('instagram');
            $table->string('youtube');
            $table->string('quick_link_1');
            $table->string('url_quicklinks_1');
            $table->string('quick_link_2');
            $table->string('url_quicklinks_2');
            $table->string('quick_link_3');
            $table->string('url_quicklinks_3');
            $table->string('quick_link_4');
            $table->string('url_quicklinks_4');
            $table->text('url_youtube');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
};
