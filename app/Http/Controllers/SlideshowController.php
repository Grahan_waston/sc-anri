<?php

namespace App\Http\Controllers;

use App\Models\Slideshow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $slideshows = Slideshow::latest(); 
        if ($request->ajax()) {
            return DataTables::of($slideshows)->addIndexColumn()
                ->addColumn('checkbox', function ($slideshow) {
                    return '<input type="checkbox" name="id" data-id="' . $slideshow->id . '">';
                })
                ->addColumn('action', function ($slideshow) {
                    if (auth()->user()->role == "admin") {
                        if ($slideshow->status == "draft") {
                            return '
                            <div class="btn-group">
                                <a href="' . route('slideshow.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-success approval"><i class="fa fa-check" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Publish"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        } else {
                            return '
                            <div class="btn-group">
                                <a href="' . route('slideshow.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger unapproval"><i class="fa fa-times" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Draft"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        }
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="' . route('slideshow.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->filter(function ($user) use ($request) {
                    if ($request->get('status') == 'draft' || $request->get('status') == 'published') {
                        $user->where('status', $request->get('status'));
                    }
                    if (!empty($request->get('search'))) {
                         $user->where(function($w) use($request){
                            $search = $request->get('search');
                            $w->orWhere('judul', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }
        return view('admin.dashboard.slideshow.slideshow_page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dashboard.slideshow.slideshow_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'tautan' => 'required',
            'image_dekstop' => 'required|image|mimes:jpeg,png,jpg',
            'image_mobile' => 'required|image|mimes:jpeg,png,jpg',
            'status' => 'required'
        ]);

        if ($request->hasFile('image_dekstop') && $request->hasFile('image_mobile')) {
            $validateData['image_dekstop'] = $request->file('image_dekstop')->store('img');
            $validateData['image_mobile'] = $request->file('image_mobile')->store('img');
            // return $validateData;
        }
        
        // dd('Registrasi Berhasil');  
        // return redirect($validateData);

        Slideshow::create($validateData);

        return redirect('/slideshow')->with('success', 'Slideshow berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_slideshow = Slideshow::find($id);
        return view('admin.dashboard.slideshow.slideshow_update', compact('data_slideshow', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'tautan' => 'required',
        ]);

        if ($request->hasFile('image_dekstop')) {
            $validateData['image_dekstop'] = $request->file('image_dekstop')->store('img');
            // return $validateData;
        }
        if ($request->hasFile('image_mobile')) {
            $validateData['image_mobile'] = $request->file('image_mobile')->store('img');
        }
        
        Slideshow::where('id', $id)->update($validateData);

        return redirect('/slideshow')->with('success', 'Slideshow berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                Slideshow::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $publication = Slideshow::find($id);
            $publication->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function approve_slideshow(Request $request, $id)
    {
        if ($request->has('data')) {
            Slideshow::whereIn('id', $request->data)->update(['status' => 'published']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Slideshow::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil Publish Data'
        ]);
    }

    public function unapprove_slideshow(Request $request, $id)
    {
        if ($request->has('data')) {
            Slideshow::whereIn('id', $request->data)->update(['status' => 'draft']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Slideshow::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil draft Data'
        ]);
    }
}
