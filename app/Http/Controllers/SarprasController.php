<?php

namespace App\Http\Controllers;

use App\Models\ProfilInstansi;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str; 

class SarprasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sarpras = Sarpras::get();

        if ($request->ajax()) {
            return DataTables::of($sarpras)->addIndexColumn()
                ->addColumn('checkbox', function ($publication) {
                    return '<input type="checkbox" name="id" data-id="' . $publication->id . '">';
                })
                ->addColumn('action', function ($publication) {
                    if (auth()->user()->role == "admin") {
                            return '
                            <div class="btn-group">
                                <a href="' . route('sarana-prasarana.edit', $publication->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="' . route('sarana-prasarana.edit', $publication->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if (Str::contains(Str::lower($row['deskripsi']), Str::lower($request->get('search')))){
                                return true;
                            }
                            return false;
                        });
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }

        return view('admin.dashboard.sarpras.sarpras_page');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dashboard.sarpras.sarpras_detail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'deskripsi' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        if ($request->hasFile('image')) {
            $validateData['image'] = $request->file('image')->store('img');
        }

        Sarpras::create($validateData);

        return redirect('/sarana-prasarana')->with('success', 'Sarana dan Prasarana berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sarpras  $sarpras
     * @return \Illuminate\Http\Response
     */
    public function show(Sarpras $sarpras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sarpras  $sarpras
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_sarpras = Sarpras::find($id);
        return view('admin.dashboard.sarpras.sarpras_update', compact('data_sarpras', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sarpras  $sarpras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'deskripsi' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $validateData['image'] = $request->file('image')->store('img');
            // return $validateData;
        }

        Sarpras::where('id', $id)->update($validateData);

        return redirect('/sarana-prasarana')->with('success', 'Sarana Prasarana berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sarpras  $sarpras
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                Sarpras::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $publication = Sarpras::find($id);
            $publication->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function approve_sarpras(Request $request, $id)
    {
        if ($request->has('data')) {
            Sarpras::whereIn('id', $request->data)->update(['status' => 'published']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Sarpras::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil Publish Data'
        ]);
    }

    public function unapprove_sarpras(Request $request, $id)
    {
        if ($request->has('data')) {
            Sarpras::whereIn('id', $request->data)->update(['status' => 'draft']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Sarpras::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil draft Data'
        ]);
    }
}
