<?php

namespace App\Http\Controllers;

use App\Models\Testimoni;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $testimoni = Testimoni::latest();
        if ($request->ajax()) {
            return DataTables::of($testimoni)->addIndexColumn()
                ->addColumn('checkbox', function ($testi) {
                    return '<input type="checkbox" name="id" data-id="' . $testi->id . '">';
                })
                ->addColumn('status', function ($publication) {
                    if ($publication->status == "draft") {
                        return '<div class="badge bg-danger">Draft</div>';
                    } else {
                        return '<div class="badge bg-success">Published</div>';
                    }
                })
                ->addColumn('action', function ($testi) {
                    if (auth()->user()->role == "admin") {
                        if ($testi->status == "draft") {
                            return '
                            <div class="btn-group">
                                <a href="' . route('testimoni.edit', $testi->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $testi->id . '" href="#!" class="btn btn-light m-1 h3 text-success approval"><i class="fa fa-check" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Publish"></i></a>
                                <a id="' . $testi->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        } else {
                            return '
                            <div class="btn-group">
                                <a href="' . route('testimoni.edit', $testi->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $testi->id . '" href="#!" class="btn btn-light m-1 h3 text-danger unapproval"><i class="fa fa-times" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Draft"></i></a>
                                <a id="' . $testi->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        }
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="' . route('testimoni.edit', $testi->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $testi->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->filter(function ($user) use ($request) {
                    if ($request->get('status') == 'draft' || $request->get('status') == 'published') {
                        $user->where('status', $request->get('status'));
                    }
                    if (!empty($request->get('search'))) {
                         $user->where(function($w) use($request){
                            $search = $request->get('search');
                            $w->orWhere('name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['checkbox', 'action', 'status'])->make(true);
        }
        return view('admin.dashboard.testimoni.testimoni_page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dashboard.testimoni.testimoni_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'jabatan' => 'required',
            'testimoni' => 'required',
            'status' => 'required'
            
        ]);

        // dd('Registrasi Berhasil');  
        // return redirect($validateData);

        Testimoni::create($validateData);

        return redirect('/testimoni')->with('success', 'Testimoni berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_testimoni = Testimoni::find($id);
        return view('admin.dashboard.testimoni.testimoni_detail', compact('data_testimoni', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'jabatan' => 'required',
            'testimoni' => 'required',
            'status' => 'required'
        ]);

        $testimoni = Testimoni::find($id);

        $testimoni->name = $request->name;
        $testimoni->jabatan = $request->jabatan;
        $testimoni->testimoni = $request->testimoni;
        $testimoni->save();

        $request->session()->flash('success', 'Update berhasil !');

        return redirect('/testimoni');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                Testimoni::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $publication = Testimoni::find($id);
            $publication->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function approve_testimoni(Request $request, $id)
    {
        if ($request->has('data')) {
            Testimoni::whereIn('id', $request->data)->update(['status' => 'published']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Testimoni::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil Publish Data'
        ]);
    }

    public function unapprove_testimoni(Request $request, $id)
    {
        if ($request->has('data')) {
            Testimoni::whereIn('id', $request->data)->update(['status' => 'draft']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Testimoni::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil draft Data'
        ]);
    }
}
