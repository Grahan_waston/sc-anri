<?php

namespace App\Http\Controllers;

use App\Models\HumanResource;
use App\Models\Jabatan;
use App\Models\Slideshow;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str; 

class HumanResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get all SDM from Model
        $SDM = HumanResource::with('jabatan')->latest();
        if ($request->ajax()) {
            return DataTables::of($SDM)->addIndexColumn()
                ->addColumn('checkbox', function ($slideshow) {
                    return '<input type="checkbox" name="id" data-id="' . $slideshow->id . '">';
                })
                ->addColumn('action', function ($slideshow) {
                    if (auth()->user()->role == "admin") {
                            return '
                            <div class="btn-group">
                                <a href="' . route('SDM.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="' . route('SDM.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->filter(function ($user) use ($request) {
                    if (!empty($request->get('search'))) {
                         $user->where(function($w) use($request){
                            $search = $request->get('search');
                            $w->orWhere('nama', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }
        //passing SDM to view
        return view('admin.dashboard.profil_SDM.profil_pejabat', compact('SDM'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dashboard.profil_SDM.profil_pejabat_add', [
            'jabatan' => Jabatan::get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'jabatan_id' => 'required',
            'keterangan' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        if ($request->hasFile('image')) {
            $validateData['image'] = $request->file('image')->store('img');
        }

        HumanResource::create($validateData);

        return redirect(route('SDM.index'))->with('success', 'Pejabat berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function show(HumanResource $humanResource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.dashboard.profil_SDM.profil_pejabat_form', [
            'jabatan' => Jabatan::get(),
            'data_pejabat' => HumanResource::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'nama' => 'required',
            'jabatan_id' => 'required',
            'keterangan' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $validateData['image'] = $request->file('image')->store('img');
        }

        HumanResource::where('id', $id)->update($validateData);

        return redirect(route('SDM.index'))->with('success', 'Pejabat berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                HumanResource::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $publication = HumanResource::find($id);
            $publication->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function approve_pejabat(Request $request, $id)
    {
        if ($request->has('data')) {
            HumanResource::whereIn('id', $request->data)->update(['status' => 'published']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        HumanResource::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil Publish Data'
        ]);
    }

    public function unapprove_pejabat(Request $request, $id)
    {
        if ($request->has('data')) {
            HumanResource::whereIn('id', $request->data)->update(['status' => 'draft']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        HumanResource::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil draft Data'
        ]);
    }
}
