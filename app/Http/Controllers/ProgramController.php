<?php

namespace App\Http\Controllers;

use App\Models\Jenis;
use App\Models\Jenjang;
use App\Models\MenuHyperlink;
use App\Models\MenuStatis;
use App\Models\Pages;
use App\Models\Program;
use App\Models\SubMenu;
use App\Models\SubMenuHyperlink;
use App\Models\Website;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class ProgramController extends Controller
{
    public $start_date;
    public $end_date;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $program = Program::latest();
        if ($request->ajax()) {
            return DataTables::of($program)->addIndexColumn()
                ->addColumn('checkbox', function ($publication) {
                    return '<input type="checkbox" name="id" data-id="' . $publication->id . '">';
                })
                ->addColumn('action', function ($publication) {
                    if (auth()->user()->role == "admin") {
                        if ($publication->status == "draft") {
                            return '
                            <div class="btn-group">
                                <a href="' . route('program-diklat.edit', $publication->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-success approval"><i class="fa fa-check" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Publish"></i></a>
                                <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        } else {
                            return '
                            <div class="btn-group">
                                <a href="' . route('program-diklat.edit', $publication->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-danger unapproval"><i class="fa fa-times" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Draft"></i></a>
                                <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        }
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="' . route('program-diklat.edit', $publication->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $publication->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->filter(function ($user) use ($request) {
                    if ($request->get('status') == 'draft' || $request->get('status') == 'published') {
                        $user->where('status', $request->get('status'));
                    }
                    if (!empty($request->get('search'))) {
                         $user->where(function($w) use($request){
                            $search = $request->get('search');
                            $w->orWhere('nama_diklat', 'LIKE', "%$search%")
                            ->orWhere('kode_diklat', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }
        return view('admin.dashboard.program_diklat.program_diklat');
    }

    public function getEvent()
    {
        $events = array();
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $bookings = Program::all();
        foreach ($bookings as $booking) {
            # code...
            if ($booking->status == 'published') {
                # code...
                $events[] = [
                    'start' => $booking->start_date,
                    'end' => $booking->end_date."T23:59:00",
                    'title' => $booking->nama_diklat,
                ];
            }
        }
        // return $events;

        return view('website.diklat.kalender_diklat', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'events' => $events,
            'jenis' => Jenis::get(),
            'jenjang' => Jenjang::get(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function search_program(Request $request)
    {
        $search = $request->get('keywords');
        $jenis = $request->get('jenis');
        $jenjang = $request->get('jenjang');
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $events = array();
        $bookings = Program::Where('nama_diklat', 'like', '%' . $search . '%')
            ->Where('jenis_id', 'like', '%' . $jenis . '%')
            ->Where('jenjang_id', 'like', '%' . $jenjang . '%')
            ->get();

        foreach ($bookings as $booking) {
            # code...
            if ($booking->status == 'published') {
                # code...
                $events[] = [
                    'start' => $booking->start_date,
                    'end' => $booking->end_date,
                    'title' => $booking->kode_diklat,
                ];
            }
        }

        return view('website.diklat.kalender_diklat_search', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'events' => $events,
            'menus' => Pages::get(),
            'jenis' => Jenis::get(),
            'jenjang' => Jenjang::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dashboard.program_diklat.program_diklat_add',  [
            'jenis' => Jenis::latest()->paginate(),
            'jenjang' => Jenjang::latest()->paginate(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'kode_diklat' => 'required',
            'jenis_id' => 'required',
            'jenjang_id' => 'required',
            'nama_diklat' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after_or_equal:start_date',
            'tempat_diklat' => 'required',
            'biaya' => 'required',
            'durasi' => 'required',
            'deskripsi' => 'required',
            'file' => 'required|file|mimes:pdf,docx',
            'status' => 'required'
        ]);

        if ($request->hasFile('file')) {
            $validateData['file'] = $request->file('file')->store('files');
        }

        Program::create($validateData);

        return redirect('/program-diklat')->with('success', 'Program Diklat berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.dashboard.program_diklat.program_diklat_update',  [
            'jenis' => Jenis::latest()->paginate(),
            'jenjang' => Jenjang::latest()->paginate(),
            'data_program' => Program::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'kode_diklat' => 'required',
            'jenis_id' => 'required',
            'jenjang_id' => 'required',
            'nama_diklat' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after_or_equal:start_date',
            'tempat_diklat' => 'required',
            'biaya' => 'required',
            'durasi' => 'required',
            'deskripsi' => 'required',
            // 'file' => 'required|file|mimes:pdf,docx',
        ]);

        if ($request->hasFile('file')) {
            $validateData['file'] = $request->file('file')->store('files');
        }

        Program::where('id', $id)->update($validateData);

        return redirect('/program-diklat')->with('success', 'Program Diklat berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        try {
            if ($request->has('data')) {
                Program::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $publication = Program::find($id);
            $publication->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function approve_program(Request $request ,$id)
    {
        if ($request->has('data')) {
            Program::whereIn('id', $request->data)->update(['status' => 'published']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Program::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil Publish Data'
        ]);
    }

    public function unapprove_program(Request $request ,$id)
    {
        if ($request->has('data')) {
            Program::whereIn('id', $request->data)->update(['status' => 'draft']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Program::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil draft Data'
        ]);
    }
    
    public function getStartDate(){
        return $this->start_date ?: date('Y-m-d',strtotime(date('Y-m-d').' - 1 year'));
    }

    public function getEndDate(){
        return $this->end_date ?: date('Y-m-d');
    }

    public function getTotalSessions()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);
        // echo $startDate;
        // echo "tes";
        // exit();
        $total = Analytics::fetchTotalVisitorsAndPageViews($periode);
        
        $total->map(function ($data) {
            return [
                'visitors' => $data['visitors']
            ];
        });
        $total_count = 0;
        for ($i = 0; $i < count($total); $i++) {
            $total_count += $total[$i]['visitors'];
        }
        return $total_count;
    }

    public function perday()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(1),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function weekly()
    {
        $analyticsBulanan = AnalyticsAnalytics::performQuery(
            Period::days(7),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function twoweeks()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(14),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function monthly()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(30),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }
}
