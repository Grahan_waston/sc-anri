<?php

namespace App\Http\Controllers;

use App\Models\Jenis;
use App\Models\Jenjang;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class JenisJenjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jenis = Jenis::latest();
        if ($request->ajax()) {
            return DataTables::of($jenis)->addIndexColumn()
                ->addColumn('checkbox', function ($job) {
                    return '<input type="checkbox" name="id" data-id="' . $job->id . '">';
                })
                ->addColumn('action', function ($job) {
                    if (auth()->user()->role == "admin") {
                            return '
                            <div class="btn-group">
                                <a href="/update-jenis/'. $job->id .'/edit" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $job->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="/update-jenis/'. $job->id .'/edit" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $job->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }
        return view('admin.dashboard.program_diklat.jenis_jenjang', [
            'jenjang' => Jenjang::latest()->paginate(5)
        ]);
    }

    Public function index_jenjang(Request $request){
        $jenjang = Jenjang::with('jenis')->latest();
        if ($request->ajax()) {
            return DataTables::of($jenjang)->addIndexColumn()
                ->addColumn('checkbox', function ($jenjang) {
                    return '<input type="checkbox" name="ids" data-id="' . $jenjang->id . '">';
                })
                ->addColumn('action', function ($jenjang) {
                    if (auth()->user()->role == "admin") {
                        return '
                            <div class="btn-group">
                                <a href="/update-jenjang/'. $jenjang->id .'/edit" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a ids="' . $jenjang->id . '" href="#!" class="btn btn-light m-1 h3 text-danger deletejenjang"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                    } else {
                        return '
                            <div class="btn-group">
                                <a href="/update-jenjang/'. $jenjang->id .'/edit" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $jenjang->id . '" href="#!" class="btn btn-light m-1 h3 text-danger deletejenjang"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }
        return view('admin.dashboard.program_diklat.jenis_jenjang', [
            'jenjang' => Jenjang::latest()->paginate(5)
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('admin.dashboard.publikasi.publikasi_form');
    }

    public function create_jenis()
    {
        return view('admin.dashboard.program_diklat.jenis_add');
    }

    public function create_jenjang()
    {
        return view('admin.dashboard.program_diklat.jenjang_add',  [
            'jenis' => Jenis::latest()->paginate()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function store_jenis(Request $request)
    {
        $validateData = $request->validate([
            'nama_jenis' => 'required',
            'status' => 'required'
        ]);

        Jenis::create($validateData);

        return redirect('/jenis-jenjang')->with('successs', 'Jenis berhasil di tambahkan');
    }

    public function store_jenjang(Request $request)
    {
        $validateData = $request->validate([
            'jenis_id' => 'required',
            'jenjang' => 'required',
            'status' => 'required'
        ]);

        Jenjang::create($validateData);

        return redirect('/jenis-jenjang')->with('success', 'Jenjang berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit_jenis($id)
    {
        $jenis = Jenis::findOrFail($id);
        return view('admin.dashboard.program_diklat.jenis_update',  [
            'jenis' => $jenis
        ]);
    }
    public function edit_jenjang($id)
    {
        $jenjang = Jenjang::find($id);
        return view('admin.dashboard.program_diklat.jenjang_update',  [
            'jenis' => Jenis::latest()->paginate(),
            'jenjang' => $jenjang
        ]);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function update_jenis(Request $request, $id)
    {
        $validateData = $request->validate([
            'nama_jenis' => 'required',
        ]);

        Jenis::where('id', $id)->update($validateData);

        return redirect('/jenis-jenjang')->with('successs', 'Jenis berhasil di update');
    }

    public function update_jenjang(Request $request, $id)
    {
        $validateData = $request->validate([
            'jenis_id' => 'required',
            'jenjang' => 'required',
        ]);

        Jenjang::where('id', $id)->update($validateData);

        return redirect('/jenis-jenjang')->with('success', 'Jenjang berhasil di update');
    }

    public function approve_jenis($id)
    {

        Jenis::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return redirect('/jenis-jenjang')->with('successs', 'Jenis diklat berhasil di publish');
    }

    public function unapprove_jenis($id)
    {

        Jenis::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return redirect('/jenis-jenjang')->with('successs', 'Jenis diklat berhasil menjadi draft');
    }

    public function approve_jenjang($id)
    {

        Jenjang::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return redirect('/jenis-jenjang')->with('success', 'Jenjang diklat berhasil di publish');
    }

    public function unapprove_jenjang($id)
    {

        Jenjang::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return redirect('/jenis-jenjang')->with('success', 'Jenjang diklat berhasil menjadi draft');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_jenis(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                Jenis::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $file = Jenis::find($id);
            $file->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Data tidak dapat dihapus',
            ]);
        }
    }

    public function destroy_jenjang(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                Jenjang::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $file = Jenjang::find($id);
            $file->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Data tidak dapat dihapus',
            ]);
        }
    }
}
