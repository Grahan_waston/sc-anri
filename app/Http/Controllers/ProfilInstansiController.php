<?php

namespace App\Http\Controllers;

use App\Models\HumanResource;
use App\Models\Jabatan;
use App\Models\MenuHyperlink;
use App\Models\MenuStatis;
use App\Models\Pages;
use App\Models\ProfilInstansi;
use App\Models\SubMenu;
use App\Models\SubMenuHyperlink;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class ProfilInstansiController extends Controller
{
    public $start_date;
    public $end_date;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard.profil_instansi.profil_page', [
            'profil' => ProfilInstansi::latest()->paginate()
        ]);
    }

    public function getStartDate(){
        return $this->start_date ?: date('Y-m-d',strtotime(date('Y-m-d').' - 1 year'));
    }

    public function getEndDate(){
        return $this->end_date ?: date('Y-m-d');
    }

    public function getTotalSessions()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);
        // echo $startDate;
        // echo "tes";
        // exit();
        $total = Analytics::fetchTotalVisitorsAndPageViews($periode);
        
        $total->map(function ($data) {
            return [
                'visitors' => $data['visitors']
            ];
        });
        $total_count = 0;
        for ($i = 0; $i < count($total); $i++) {
            $total_count += $total[$i]['visitors'];
        }
        return $total_count;
    }

    public function perday()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(1),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function weekly()
    {
        $analyticsBulanan = AnalyticsAnalytics::performQuery(
            Period::days(7),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function twoweeks()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(14),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function monthly()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(30),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function sejarah()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.profil.sejarah', [
            'menu' => MenuStatis::get(),
            'mainmenu' => Pages::get(),
            'submenu' => SubMenu::get(),
            'sejarah' => ProfilInstansi::find(1),
            'website' => $website,
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function visi_misi()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.profil.visi_misi', [
            'menu' => MenuStatis::get(),
            'mainmenu' => Pages::get(),
            'submenu' => SubMenu::get(),
            'visi_misi' => ProfilInstansi::find(2),
            'website' => $website,
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function tugas_fungsi()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.profil.tugas_fungsi', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'tugas_fungsi' => ProfilInstansi::find(3),
            'website' => $website,
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function struktur_organisasi()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.profil.struktur_organisasi', [
            'menu' => MenuStatis::get(),
            'mainmenu' => Pages::get(),
            'submenu' => SubMenu::get(),
            'struktur_organisasi' => ProfilInstansi::find(4),
            'website' => $website,
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function sumber_daya_manusia()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.profil.sumber_daya_manusia', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'sumber_daya_manusia' => ProfilInstansi::find(5),
            'website' => $website,
            'pejabat' => HumanResource::get(),
            'jabatan' => Jabatan::get(),
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function maklumat_layanan()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.profil.maklumat_layanan', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'maklumat_layanan' => ProfilInstansi::find(6),
            'website' => $website,
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProfilInstansi  $profilInstansi
     * @return \Illuminate\Http\Response
     */
    public function show(ProfilInstansi $profilInstansi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProfilInstansi  $profilInstansi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_profil = ProfilInstansi::find($id);
        return view('admin.dashboard.profil_instansi.profil_form', compact('data_profil', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProfilInstansi  $profilInstansi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'deskripsi' => 'required',
        ]);

        // dd($validateData);
        // return redirect($validateData);

        ProfilInstansi::where('id', $id)->update($validateData);

        return redirect('/profil-instansi')->with('success', 'Profil Instansi berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProfilInstansi  $profilInstansi
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfilInstansi $profilInstansi)
    {
        //
    }
}
