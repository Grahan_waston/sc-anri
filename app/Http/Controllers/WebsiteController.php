<?php

namespace App\Http\Controllers;

use App\Models\FileDownload;
use App\Models\Jenis;
use App\Models\Layanan;
use App\Models\MenuHyperlink;
use App\Models\MenuStatis;
use App\Models\Pages;
use App\Models\Program;
use App\Models\Publication;
use App\Models\Sarpras;
use App\Models\Section4;
use App\Models\Slideshow;
use App\Models\SubMenu;
use App\Models\SubMenuHyperlink;
use App\Models\Testimoni;
use App\Models\User;
use App\Models\Website;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;

class WebsiteController extends Controller
{
    public $start_date;
    public $end_date;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $website = Website::find(1);

        return view('admin.dashboard.konfigurasi.konfigurasi_situs', [
            'website' => $website,
        ]);
    }

    public function website_kontak()
    {
        $website = Website::find(1);
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();

        return view('website.kontak.kontak_kami', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'website' => $website,
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function website_sarpras()
    {
        $sarpras = Sarpras::get();
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();

        return view('website.sarpras.sarpras', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'sarpras' => $sarpras,
            'website' => Website::find(1),
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function view_menu($n_m)
    {
        // return ;
        $pages = Pages::where('nama_menu', $n_m)->get()->first();
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();

        if (!$pages) abort(404);

        $sarpras = Sarpras::get();

        return view('website.menu.view_menu', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'pages' => $pages,
            'mainmenu' => Pages::get(),
            'sarpras' => $sarpras,
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function view_submenu($url)
    {
        $submenu = SubMenu::where('url', $url)->get()->first();
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        if (!$submenu) abort(404);
        $sarpras = Sarpras::get();

        return view('website.menu.view_submenu', [
            'menu' => MenuStatis::get(),
            'submenus' => $submenu,
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'sarpras' => $sarpras,
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function program_diklat()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.diklat.program_diklat', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'programs' => Program::latest()->paginate(3),
            'mainmenu' => Pages::get(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function program_detail($kode_diklat)
    {
        // return $program = Program::where('kode_diklat', $kode_diklat)->get()->first();
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.diklat.program_detail', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'programs_diklat' => Program::latest()->paginate(),
            'programs' => Program::where('kode_diklat', $kode_diklat)->get()->first(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function berita_detail($slug)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.publikasi.detail_berita', [
            'publikasi' => Publication::where('slug', $slug)->get()->first(),
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'news' => Publication::where('category_id', 1)->get(),
            'info' => Publication::where('category_id', 2)->get(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function infografis_detail($slug)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        return view('website.publikasi.detail_infografis', [
            'publikasi' => Publication::where('slug', $slug)->get()->first(),
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            // 'news' => Publication::where('category_id', 1)->get(),
            'info' => Publication::where('category_id', 2)->get(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function kalender()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        // return $program = Program::where('kode_diklat', $kode_diklat)->get()->first();
        return view('website.diklat.kalender_diklat', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'programs_diklat' => Program::latest()->paginate(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function artikel()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        // return $program = Program::where('kode_diklat', $kode_diklat)->get()->first();
        return view('website.publikasi.article', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'publications' => Publication::latest()->paginate(),
            'articles' => FileDownload::where('category_id', 3)->paginate(5),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function berita()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        // return $program = Program::where('kode_diklat', $kode_diklat)->get()->first();
        return view('website.publikasi.berita', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'publications' => Publication::latest()->paginate(3),
            'news' => Publication::where('category_id', 1)->paginate(3),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function infografis()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        // return $program = Program::where('kode_diklat', $kode_diklat)->get()->first();
        return view('website.publikasi.infografis', [
            'menu' => MenuStatis::get(),
            'mainmenu' => Pages::get(),
            'submenu' => SubMenu::get(),
            'publications' => Publication::latest()->paginate(),
            'infografis' => Publication::where('category_id', 2)->paginate(3),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function pengumuman()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        // return $program = Program::where('kode_diklat', $kode_diklat)->get()->first();
        return view('website.publikasi.pengumuman', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'publications' => Publication::latest()->paginate(),
            'pengumuman' => FileDownload::where('category_id', 4)->get(),
            'menus' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function menu_navbar()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $menus = Pages::latest();
        $sarpras = Sarpras::get();

        return view('website.header_footer', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'sarpras' => $sarpras,
            'website' => Website::find(1),
            'menus' => $menus,
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function website_beranda()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $website = Website::find(1);
        return view('website.beranda', [
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'website' => $website,
            'mainmenu' => Pages::get(),
            'section' => Section4::latest()->first(),
            'testimoni' => Testimoni::get(),
            'slideshow' => Slideshow::where('status', 'published')->get(),
            'service' => Layanan::all(),
            'menus' => Pages::get(),
            'program' => Program::latest()->get(),
            'berita' => Publication::latest()->get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
         ]);
    }

    public function search(Request $request)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $search = $request->get('keywords');
        $publikasi = Publication::where('title', 'like', '%' . $search . '%')->latest()->paginate(3, ['*'], 'publikasi')->withquerystring();
        $file = FileDownload::where('title', 'like', '%' . $search . '%')->paginate(3, ['*'], 'file')->withquerystring();
        $program = Program::where('nama_diklat', 'like', '%' . $search . '%')->paginate(3, ['*'], 'program')->withquerystring();

        return view('website.search', [
            'publikasi' => $publikasi,
            'file' => $file,
            'program' => $program,
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function search_berita(Request $request)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $search = $request->get('keywords');
        $publikasi = Publication::where('category_id', 1)->Where('title', 'like', '%' . $search . '%')->paginate(3, ['*'], 'publikasi')->withquerystring();

        return view('website.publikasi.berita_search', [
            'publikasi' => $publikasi,
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function search_infografis(Request $request)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $search = $request->get('keywords');
        $publikasi = Publication::where('category_id', 2)->Where('title', 'like', '%' . $search . '%')->paginate(3, ['*'], 'publikasi')->withquerystring();

        return view('website.publikasi.infografis_search', [
            'publikasi' => $publikasi,
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'mainmenu' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function search_artikel(Request $request)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $search = $request->get('keywords');
        $year = $request->get('tahun');
        $publikasi = FileDownload::where('category_id', 3)
            ->Where('title', 'like', '%' . $search . '%')
            ->Where('year', 'like', '%' . $year . '%')
            ->paginate(3, ['*'], 'publikasi')->withquerystring();

        return view('website.publikasi.article_search', [
            'publikasi' => $publikasi,
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'articles' => FileDownload::where('category_id', 3)->paginate(5),
            'mainmenu' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function search_pengumuman(Request $request)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $search = $request->get('keywords');
        $year = $request->get('tahun');
        $publikasi = FileDownload::where('category_id', 4)
            ->Where('title', 'like', '%' . $search . '%')
            ->Where('year', 'like', '%' . $year . '%')
            ->paginate(3, ['*'], 'publikasi')->withquerystring();

        return view('website.publikasi.pengumuman_search', [
            'publikasi' => $publikasi,
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'pengumuman' => FileDownload::where('category_id', 4)->get(),
            'mainmenu' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function search_program(Request $request)
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $search = $request->get('keywords');
        $jenis = $request->get('jenis');
        $jenjang = $request->get('jenjang');
        $program = Program::where('nama_diklat', 'like', '%' . $search . '%')
            ->Where('jenis_id', 'like', '%' . $jenis . '%')
            ->Where('jenjang_id', 'like', '%' . $jenjang . '%')
            ->paginate(3, ['*'], 'program')->withquerystring();

        return view('website.diklat.program_diklat_search', [
            'program' => $program,
            'menu' => MenuStatis::get(),
            'submenu' => SubMenu::get(),
            'programss' => Program::get(),
            'mainmenu' => Pages::get(),
            'website' => Website::find(1),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    public function getStartDate(){
        return $this->start_date ?: date('Y-m-d',strtotime(date('Y-m-d').' - 1 year'));
    }

    public function getEndDate(){
        return $this->end_date ?: date('Y-m-d');
    }

    public function getTotalSessions()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);
        // echo $startDate;
        // echo "tes";
        // exit();
        $total = Analytics::fetchTotalVisitorsAndPageViews($periode);
        
        $total->map(function ($data) {
            return [
                'visitors' => $data['visitors']
            ];
        });
        $total_count = 0;
        for ($i = 0; $i < count($total); $i++) {
            $total_count += $total[$i]['visitors'];
        }
        return $total_count;
    }

    public function getHits()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);

        $hits = Analytics::performQuery(
            $periode,
            'ga:hits',
        );

        $rows = [];
        if (!empty($hits->rows)) {
            foreach ($hits->rows as $key => $value) {
                $rows[$key]['hits'] = intval($value[0]);
            }
        }

        return $rows;
    }

    public function getUserType()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);

        $userType = Analytics::performQuery(
            $periode,
            'ga:percentNewSessions',
        );

        $rows = [];
        if (!empty($userType->rows)) {
            foreach ($userType->rows as $key => $value) {
                $rows[$key]['userNew'] = round($value[0], 2);
                $rows[$key]['userOld'] = round(100 - $value[0], 2);
            }
        }

        return $rows;
    }

    public function getPagesPerSessions()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);

        $userType = Analytics::performQuery(
            $periode,
            'ga:pageviewsPerSession',
        );

        $rows = [];
        if (!empty($userType->rows)) {
            foreach ($userType->rows as $key => $value) {
                $rows[$key]['page'] = round($value[0], 2);
            }
        }

        return $rows;
    }

    public function getAvgSessions()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);

        $userType = Analytics::performQuery(
            $periode,
            'ga:avgSessionDuration',
        );

        $rows = [];
        if (!empty($userType->rows)) {
            foreach ($userType->rows as $key => $value) {
                $rows[$key]['seconds'] = intval($value[0]);
            }
        }

        return $rows;
    }

    public function getDataKota()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);

        $total = $this->getTotalSessions();
        $analyticsBulanan = Analytics::performQuery(
            $periode,
            'ga:users',
            [
                'metrics' => 'ga:users',
                // 'dimensions' => 'ga:yearMonth',
                'dimensions' => 'ga:city',
                'sort' => '-ga:users',
                'max-results' => 10
            ]
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['kota'] = $value[0];
                $rows[$key]['pengunjung'] = $value[1];
                $rows[$key]['persentase'] = $total != 0 ? round(($value[1] / $total) * 100, 2) : 0;
                $rows[$key]['total'] = $total;
            }
        }

        return $rows;
    }

    public function getDataperMonth()
    {

        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);

        $analyticsBulanan = Analytics::performQuery(
            $periode,
            'ga:users',
            [
                'dimensions' => 'ga:Month',
                'metrics' => 'ga:users',
            ]
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['bulan'] = $value[0];
                $rows[$key]['pengunjung'] = intval($value[1]);
            }
        }

        return $rows;
    }

    public function perday()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(1),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function weekly()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(7),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function twoweeks()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(14),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function monthly()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(30),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function dashboard_counter(Request $request)
    {
        $this->start_date = $request->start_date;
        $this->end_date = $request->end_date;
        $perday_count = $this->perday();
        $weekly =  $this->weekly();
        $twoweeks = $this->twoweeks();
        $monthly = $this->monthly();
        $analyticsBulanan = $this->getDataKota();
        $total = $this->getTotalSessions();
        $dataPerMonth = $this->getDataperMonth();
        $dataHits = $this->getHits();
        $dataUserTypes = $this->getUserType();
        $numberAvg = $this->getAvgSessions();
        $page = $this->getPagesPerSessions();

        // Create time
        $seconds = isset($numberAvg[0]['seconds']) ? intval($numberAvg[0]['seconds'] % 60) : 0; 
        $total_minutes = intval($numberAvg[0]['seconds'] / 60);
        $minutes = $total_minutes % 60;

        return view('admin.dashboard.dashboard_home', [
            'berita' => Publication::where('category_id', 1)->count(),
            'infografis' => Publication::where('category_id', 2)->count(),
            'artikel' => FileDownload::where('category_id', 3)->count(),
            'pengumuman' => FileDownload::where('category_id', 4)->count(),
            'perday' => $perday_count,
            'weekly' => $weekly,
            'twoweeks' => $twoweeks,
            'monthly' => $monthly,
            'kota' => $analyticsBulanan,
            'users' => User::where('status', 1)->count(),
            'total_users' => $total,
            'dataPerMonth' => $dataPerMonth,
            'dataHits' => $dataHits,
            'userTypes' => $dataUserTypes,
            'times' => "$minutes:$seconds",
            'page' => $page,
        ]);
    }

    public function dashboard_search(Request $request)
    {
        $this->start_date = $request->start_date;
        $this->end_date = $request->end_date;
        $perday_count = $this->perday();
        $weekly =  $this->weekly();
        $twoweeks = $this->twoweeks();
        $monthly = $this->monthly();
        $analyticsBulanan = $this->getDataKota();
        $total = $this->getTotalSessions();
        $dataPerMonth = $this->getDataperMonth();
        $dataHits = $this->getHits();
        $dataUserTypes = $this->getUserType();
        $numberAvg = $this->getAvgSessions();
        $page = $this->getPagesPerSessions();
        // Create time
        $seconds = isset($numberAvg[0]['seconds']) ? intval($numberAvg[0]['seconds'] % 60) : 0; 
        $total_minutes = isset($numberAvg[0]['seconds']) ? intval($numberAvg[0]['seconds'] / 60) : 0;
        $minutes = $total_minutes % 60;

        return view('admin.dashboard.dashboard_home', [
            'berita' => Publication::where('category_id', 1)->count(),
            'infografis' => Publication::where('category_id', 2)->count(),
            'artikel' => FileDownload::where('category_id', 3)->count(),
            'pengumuman' => FileDownload::where('category_id', 4)->count(),
            'perday' => $perday_count,
            'weekly' => $weekly,
            'twoweeks' => $twoweeks,
            'monthly' => $monthly,
            'kota' => $analyticsBulanan,
            'users' => User::where('status', 1)->count(),
            'total_users' => $total,
            'dataPerMonth' => $dataPerMonth,
            'dataHits' => $dataHits,
            'userTypes' => $dataUserTypes,
            'times' => "$minutes:$seconds",
            'page' => $page,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function show(Website $website)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.dashboard.konfigurasi.konfigurasi_situs', [
            'website' => Website::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'nama_website' => 'required',
            'alamat' => 'required',
            'no_telfon' => 'required',
            'no_whatsapp' => 'required',
            'email_pertama' => 'required',
            'email_kedua' => 'required',
            'maps' => 'required',
            'facebook' => 'required',
            'twitter' => 'required',
            'instagram' => 'required',
            'youtube' => 'required',
            'url_youtube' => 'required',
            'quick_link_1' => 'required',
            'url_quicklinks_1' => 'required',
            'quick_link_2' => 'required',
            'url_quicklinks_2' => 'required',
            'quick_link_3' => 'required',
            'url_quicklinks_3' => 'required',
            'quick_link_4' => 'required',
            'url_quicklinks_4' => 'required',
        ]);

        Website::where('id', $id)->update($validateData);

        // $request->session()->flash('success', 'Update berhasil !');

        return redirect()->back()->with('success', 'Update berhasil !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function destroy(Website $website)
    {
        //
    }
}
