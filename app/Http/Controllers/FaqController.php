<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\MenuHyperlink;
use App\Models\MenuStatis;
use App\Models\Pages;
use App\Models\SubMenu;
use App\Models\SubMenuHyperlink;
use App\Models\Testimoni;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class FaqController extends Controller
{
    public $start_date;
    public $end_date;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $faq = Faq::latest();
        if ($request->ajax()) {
            return DataTables::of($faq)->addIndexColumn()
                ->addColumn('checkbox', function ($slideshow) {
                    return '<input type="checkbox" name="id" data-id="' . $slideshow->id . '">';
                })
                ->addColumn('action', function ($slideshow) {
                    if (auth()->user()->role == "admin") {
                        if ($slideshow->status == "draft") {
                            return '
                            <div class="btn-group">
                                <a href="' . route('faq.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-success approval"><i class="fa fa-check" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Publish"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        } else {
                            return '
                            <div class="btn-group">
                                <a href="' . route('faq.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger unapproval"><i class="fa fa-times" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Draft"></i></a>
                                <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                            </div>
                            ';
                        }
                    } else {
                        return '
                        <div class="btn-group">
                            <a href="' . route('faq.edit', $slideshow->id) . '" class="btn btn-light m-1 h3 text-primary"><i class="fas fa-file-signature" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"></i></a>
                            <a id="' . $slideshow->id . '" href="#!" class="btn btn-light m-1 h3 text-danger delete"><i class="far fa-trash-alt" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete"></i></a>
                        </div>
                        ';
                    }
                })
                ->filter(function ($user) use ($request) {
                    if ($request->get('status') == 'draft' || $request->get('status') == 'published') {
                        $user->where('status', $request->get('status'));
                    }
                    if (!empty($request->get('search'))) {
                         $user->where(function($w) use($request){
                            $search = $request->get('search');
                            $w->orWhere('judul', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['checkbox', 'action'])->make(true);
        }
        return view('admin.dashboard.faq.faq_page');
    }

    public function website()
    {
        $perday_count = $this->perday();
        $monthly = $this->monthly();
        $total = $this->getTotalSessions();
        $website = DB::table('websites')->find(1);

        return view('website.faq.faq', [
            'menu' => MenuStatis::get(),
            'mainmenu' => Pages::get(),
            'submenu' => SubMenu::get(),
            'website' => $website,
            'faq' => Faq::get(),
            'testimoni' => Testimoni::oldest(),
            'menus' => Pages::get(),
            'menu_hyperlink' => MenuHyperlink::latest()->get(),
            'submenu_hyperlink' => SubMenuHyperlink::latest()->get(),
            'daily' => $perday_count,
            'monthly' => $monthly,
            'total' => $total,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faq = Faq::all();
        return view('admin.dashboard.faq.faq_form', compact('faq'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'status' => 'required',
            'judul' => 'required',
            'text' => 'required',
        ]);

        // dd('Registrasi Berhasil');

        Faq::create($validateData);

        return redirect('/faq')->with('success', 'FAQ berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_faq = Faq::find($id);
        return view('admin.dashboard.faq.form_update
        ', compact('data_faq', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'judul' => 'required',
            'text' => 'required',
        ]);

        $user = Faq::find($id);

        $user->judul = $request->judul;
        $user->text = $request->text;
        $user->save();

        // $request->session()->flash('success', 'Update berhasil !');

        return redirect('/faq')->with('success', 'Update berhasil !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            if ($request->has('data')) {
                Faq::whereIn('id', $request->data)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berhasil Dihapus'
                ]);
            }
            $faq = Faq::find($id);
            $faq->delete();
            return response()->json([
                'success'   => true,
                'message'   => 'Berhasil Menghapus Data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function approve_faq(Request $request, $id)
    {
        if ($request->has('data')) {
            Faq::whereIn('id', $request->data)->update(['status' => 'published']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Faq::where('id', $id)
            ->where('status', 'draft')
            ->update(['status' => 'published']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil Publish Data'
        ]);
    }

    public function unapprove_faq(Request $request, $id)
    {
        if ($request->has('data')) {
            Faq::whereIn('id', $request->data)->update(['status' => 'draft']);
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus'
            ]);
        }
        Faq::where('id', $id)
            ->where('status', 'published')
            ->update(['status' => 'draft']);

        return response()->json([
            'success'   => true,
            'message'   => 'Berhasil draft Data'
        ]);
    }

    public function getStartDate(){
        return $this->start_date ?: date('Y-m-d',strtotime(date('Y-m-d').' - 1 year'));
    }

    public function getEndDate(){
        return $this->end_date ?: date('Y-m-d');
    }

    public function getTotalSessions()
    {
        $startDate = Carbon::parse($this->getStartDate());
        $endDate = Carbon::parse($this->getEndDate());
        $periode = Period::create($startDate,$endDate);
        // echo $startDate;
        // echo "tes";
        // exit();
        $total = Analytics::fetchTotalVisitorsAndPageViews($periode);
        
        $total->map(function ($data) {
            return [
                'visitors' => $data['visitors']
            ];
        });
        $total_count = 0;
        for ($i = 0; $i < count($total); $i++) {
            $total_count += $total[$i]['visitors'];
        }
        return $total_count;
    }

    public function perday()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(1),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function weekly()
    {
        $analyticsBulanan = AnalyticsAnalytics::performQuery(
            Period::days(7),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function twoweeks()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(14),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }

    public function monthly()
    {
        $analyticsBulanan = Analytics::performQuery(
            Period::days(30),
            'ga:users',
        );

        $rows = [];
        if (!empty($analyticsBulanan->rows)) {
            foreach ($analyticsBulanan->rows as $key => $value) {
                $rows[$key]['pengunjung'] = intval($value[0]);
            }
        }
        return $rows;
    }
}
